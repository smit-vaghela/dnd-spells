import "./components/User/Login.css";
import "./components/Home/Home.css";
import Navbar from "./components/NavBar";
import Nav from "./components/Nav";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Nav />
    </div>
  );
}

export default App;
