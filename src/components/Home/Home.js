import React, { useEffect } from 'react';
import { onAuthStateChanged } from "firebase/auth";
import { auth } from '../../firbaseConfig';
import { useNavigate } from 'react-router-dom';

function Home() {
  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, user => {
      if (!user) {
        // User is not signed in, redirect to /login
        navigate('/login');
      }
    });

    // Clean up the subscription
    return () => unsubscribe();
  }, [navigate]);

  return (
    <div>
      <div className='relative p-4 flex w-full justify-evenly'>
        <div className='character-list shadow-md'>
          <h2 className='text-2xl font-semibold'>My Characters</h2>
            <button className='character shadow-md mx-4'>
              Robin Clef - Bard Level 4
            </button>
            <button className='character shadow-md mx-4'>
            Quinn - Paladin Level 6 / Warlock Level 1
            </button>
            <button className='character shadow-md mx-4'>
              Robin Clef - Bard Level 4
            </button>
            <button className='character shadow-md mx-4'>
              Sonny - Barbarian Level 2
            </button>
            <button className='character shadow-md mx-4'>
              Robin Clef - Bard Level 4
            </button>
            <button className='character shadow-md mx-4'>
            Quinn - Paladin Level 6 / Warlock Level 1
            </button>
            <button className='character shadow-md mx-4'>
              Robin Clef - Bard Level 4
            </button>
        </div>   
        <div className='flex flex-col justify-center items-center gap-5'>
          <button className='new-character-button shadow-md'>Create New Character</button>
          <button className='new-character-button shadow-md'>Create New Spell</button>
        </div>
      </div>
    </div>
  );
}

export default Home;