import { Routes, Route } from "react-router-dom";
import Home from "./Home/Home";
import NotFound from "./Extras/NotFound";
import Login from "./User/Login";
import Register from "./User/Register";
import ForgotPass from "./User/ForgotPass";
const Nav = () => {
    return (
      <div>
  <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/login" element={<Login />} />
    <Route path="/register" element={<Register />} />
    <Route path="/forgot-password" element={<ForgotPass />} />
    <Route path="*" element={<NotFound />} /> {/* Add this line */}
  </Routes>
      </div>
    );
  };
  export default Nav;

