import React, { useState, useEffect } from 'react';
import { FaUser } from "react-icons/fa";
import { signOut, onAuthStateChanged } from "firebase/auth";
import { auth } from '../firbaseConfig';

const Navbar = () => {
  // State to hold the current user
  const [currentUser, setCurrentUser] = useState(null);

  // Handle user sign-out
  const handleLogout = () => {
    signOut(auth).then(() => {
      console.log("User signed out successfully.");
    }).catch((error) => {
      console.error("Sign out error:", error);
    });
  };

  // Listen for authentication state changes
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in
        setCurrentUser(user);
      } else {
        // User is signed out
        setCurrentUser(null);
      }
    });

    // Clean up the subscription on unmount
    return () => unsubscribe();
  }, []);

  return (
    <div className='navbar-container shadow-lg mx-5 mt-3'>
        <a className='text-4xl hover:cursor-pointer font-semibold'>Dungeons and Dragons</a>
        <div className='navbar-controls flex justify-around gap-10'>
            {currentUser ? (
              <>
                <p>{currentUser.email}</p>
                <button className=' block'><FaUser  size={28}/></button>
                <button className=' block' onClick={handleLogout}>Logout</button>
              </>
            ) : (
              <a href='login'>Login</a>
            )}
        </div>
    </div>
  );
}

export default Navbar;
