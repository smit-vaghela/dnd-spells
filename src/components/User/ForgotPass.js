import React, { useState } from 'react';
import { sendPasswordResetEmail } from "firebase/auth";
import { auth } from '../../firbaseConfig'; // Adjust this import path to where your Firebase config and initialization are located.

function ForgotPass() {
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      await sendPasswordResetEmail(auth, email);
      setMessage('Check your email for the password reset link.');
      setError(''); // Clear any previous errors
    } catch (error) {
      setError("Failed to send password reset email. Please check the email provided and try again.");
      setMessage(''); // Clear any previous success message
      console.error("Error sending password reset email:", error);
    }
  };

  return (
    <div className="login-container">
      <form onSubmit={handleSubmit} className="login-form shadow-lg">
        <h2 className='text-center font-semibold text-4xl'>Forgot Password</h2>
        {message && <div className="success-message">{message}</div>}
        {error && <div className="error-message">{error}</div>}
        <div className="form-field">
          <label htmlFor="email" className='font-medium mr-5'>Email</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <button type="submit" className='mt-5 form-button'>Send Reset Email</button>
      </form>
    </div>
  );
}

export default ForgotPass;
