import React, { useEffect, useState } from 'react';
import { auth } from '../../firbaseConfig'; // Adjust the path based on where you placed the firebase-config file
import { signInWithEmailAndPassword, setPersistence, browserLocalPersistence, browserSessionPersistence } from "firebase/auth";
import { onAuthStateChanged } from "firebase/auth";
import { useNavigate } from 'react-router-dom';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  const [rememberMe, setRememberMe] = useState(false);
  const [error, setError] = useState('');
  const navigate = useNavigate();


  const handleSubmit = (e) => {
    e.preventDefault();
    const persistenceType = rememberMe ? browserLocalPersistence : browserSessionPersistence;

    setPersistence(auth, persistenceType)
      .then(() => {
        return signInWithEmailAndPassword(auth, username, password);
      })
      .then((userCredential) => {
        console.log('Login successful', userCredential.user);
        setLoggedIn(true);
        navigate('/'); // Adjust the path as necessary for your Home page
      })
      .catch((error) => {
        setError("Failed to log in: " + error.message);
        console.error(error);
      });
  };

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        console.log("User is signed in:", user);
        setLoggedIn(true);
        navigate('/'); // Optionally navigate to Home page if already logged in
      } else {
        console.log("No user is signed in.");
        setLoggedIn(false);
      }
    });
  
    return () => unsubscribe();
  }, [navigate]); 

  return (
    <>
      <div className="login-container">
        <form onSubmit={handleSubmit} className="login-form shadow-lg">
          <h2 className='text-center font-semibold text-4xl'>Login</h2>
          {error && <p className="error-message">{error}</p>}
          <div className="form-field">
            <label htmlFor="username" className='font-medium mr-5'>Username</label>
            <input
              type="text"
              id="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="form-field">
            <label htmlFor="password" className='font-medium mr-5'>Password</label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="form-field">
            <input
              type="checkbox"
              id="rememberMe"
              checked={rememberMe}
              onChange={(e) => setRememberMe(e.target.checked)}
            />
            <label htmlFor="rememberMe" className='ml-2 rem'>Remember Me</label>
          </div>
          <a href="/forgot-password" className='form-link text-center'>Forgot Password?</a>
            <button type="submit" className='form-button'>Submit</button>
          <a href="/register" className='form-link text-center'>Register User</a>
        </form>
      </div>
    </>
  );
}

export default Login;
