import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from '../../firbaseConfig'; // Adjust the path based on where you placed the firebase-config file

function Register() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [confirmEmail, setConfirmEmail] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setError('Passwords do not match');
      return;
    }

    if (email !== confirmEmail) {
      setError('Emails do not match');
      return;
    }

    try {
      const userCredential = await createUserWithEmailAndPassword(auth, email, password);
      console.log('Registration successful', userCredential.user);
      navigate('/'); // Navigate to Home page or wherever you prefer after successful registration
    } catch (error) {
      setError("Failed to register: " + error.message);
      console.error(error);
    }
  };

  return (
    <div className="login-container">
      <form onSubmit={handleSubmit} className="login-form shadow-lg">
        <h2 className='text-center font-semibold text-4xl'>Register</h2>
        {error && <p className="error-message">{error}</p>}
        <div className="form-field">
          <label htmlFor="email" className='font-medium mr-5'>Email</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="form-field">
          <label htmlFor="confirmEmail" className='font-medium mr-5'>Confirm Email</label>
          <input
            type="email"
            id="confirmEmail"
            value={confirmEmail}
            onChange={(e) => setConfirmEmail(e.target.value)}
          />
        </div>
        <div className="form-field">
          <label htmlFor="password" className='font-medium mr-5'>Password</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="form-field">
          <label htmlFor="confirmPassword" className='font-medium mr-5'>Confirm Password</label>
          <input
            type="password"
            id="confirmPassword"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </div>
        <button type="submit" className='mt-5 form-button'>Register</button>
        <a href="/login" className='form-link text-center'>Back to Login</a>
      </form>
    </div>
  );
}

export default Register;
