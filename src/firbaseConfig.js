import firebase from 'firebase/compat/app';
import { getAuth } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBzxs-P4frCdHkplA-z-t3R0g6ZzV3UZWE",
    authDomain: "dnd-spell.firebaseapp.com",
    projectId: "dnd-spell",
    storageBucket: "dnd-spell.appspot.com",
    messagingSenderId: "315429128588",
    appId: "1:315429128588:web:afa7924c060b8264ca2ea9"
  };

if(!firebase.apps.length){
    var app = firebase.initializeApp(firebaseConfig);
}
const auth = getAuth(app);
export { firebase, auth };
